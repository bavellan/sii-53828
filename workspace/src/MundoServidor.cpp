
#include <fstream>
#include "MundoServidor.h"// Mundo.cpp
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include"Puntos.h"
#include <signal.h>
#include <pthread.h>


void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador()
{

	//Fifo cliente servidor abierto en lectura

	//FifoCliente_Servidor=open("/tmp/FifoCliente_Servidor",O_RDONLY);


     while (1) {
            usleep(10);
            char cad[100];
           // read(FifoCliente_Servidor, cad, sizeof(cad));
           Comunicacion.Receive(cad,sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
	   
      }
}

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	close(fd);
        unlink("/tmp/logger");
	//close (FifoServidor_Cliente);
	//close (FifoCliente_Servidor);
	
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	puntos punt;
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	
	
	
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	
	esfera.radio-=0.005;
	
	if(esfera.radio<=0.1)
	esfera.radio=0.5;
	
	if(fondo_izq.Rebota(esfera))
	 {
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		punt.jugadoru=puntos1;
	        punt.jugadord=puntos2;
		punt.puntua=2;
	write(fd, &punt, sizeof(punt));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		punt.jugadoru=puntos1;
	        punt.jugadord=puntos2;
		punt.puntua=1;
	write(fd, &punt, sizeof(punt));
	}
	
	
	
	//Cierre del programa tras lograr los 3 puntos.
	if(puntos1==3)
	{
	printf("Victoria para el jugador 1\n");
	exit(1);
	}
	if(puntos2==3)
	{
	printf("Victoria para el jugador 2\n");
	exit(1);
	}

////Transferencia de posiciones y puntos cliente servidor
	
	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d",
		esfera.centro.x,esfera.centro.y, 
		jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,
		jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, 
		puntos1, puntos2);
	//write(FifoServidor_Cliente,cad,sizeof(cad));
	Comunicacion.Send(cad,sizeof(cad));
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	
}

void CMundoServidor::Init()
{	
char pal[100];
char ip[]="127.0.0.1";
//Conexion con socket
if(Conexion.InitServer(ip,8000)==-1)
{
printf("error de apertura");
}

//Recibe nombre del cliente
Comunicacion=Conexion.Accept();
Comunicacion.Receive(pal,sizeof(pal));
printf("%s Conectado\n",pal);

//Escritura a traves de logger
fd=open("/tmp/logger",O_WRONLY);


//Fifo servidor cliente abierto en escritura
	//FifoServidor_Cliente=open("/tmp/FifoServidor_Cliente",O_WRONLY);
	
// THREAD

	pthread_attr_init(&atrib);
	pthread_attr_setdetachstate(&atrib, PTHREAD_CREATE_JOINABLE);

	pthread_create(&thid1, &atrib, hilo_comandos, this);


	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

}

