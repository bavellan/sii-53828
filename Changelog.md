##[v1.1] 15-10-2020:  
-Comentarios en las cabeceras de los cpp.
##[v1]   15-10-2020:  
-Creacion del changelog.
##[v2]   29-10-2020: 
 -Se dota de movimiento a la esfera y a las raquetas.
 -La esfera reduce su tamaño.
##[v3]   3-12-2020:   
-Adicion de comunicacion entre procesos.
##[v4]   16-12-2020:   
-Se establece comunicacion entre cliente y servidor
